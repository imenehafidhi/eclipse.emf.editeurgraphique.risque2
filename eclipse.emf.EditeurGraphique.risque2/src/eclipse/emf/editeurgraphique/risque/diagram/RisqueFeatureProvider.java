package eclipse.emf.editeurgraphique.risque.diagram;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IPictogramElementContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.ui.features.DefaultFeatureProvider;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Activity;
import eclipse.emf.editeurgraphique.model.editeurgraphique.Branch;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EndEvent;
import eclipse.emf.editeurgraphique.model.editeurgraphique.Fork;
import eclipse.emf.editeurgraphique.model.editeurgraphique.Join;
import eclipse.emf.editeurgraphique.model.editeurgraphique.Merge;
import eclipse.emf.editeurgraphique.model.editeurgraphique.SequenceFlow;
import eclipse.emf.editeurgraphique.model.editeurgraphique.StartEvent;
import eclipse.emf.editeurgraphique.risque.features.AddActivity;
import eclipse.emf.editeurgraphique.risque.features.AddJoin;
import eclipse.emf.editeurgraphique.risque.features.AddConnection;
import eclipse.emf.editeurgraphique.risque.features.AddEndEvent;
import eclipse.emf.editeurgraphique.risque.features.AddBranch;
import eclipse.emf.editeurgraphique.risque.features.AddMerge;
import eclipse.emf.editeurgraphique.risque.features.AddFork;
import eclipse.emf.editeurgraphique.risque.features.AddStartEvent;
import eclipse.emf.editeurgraphique.risque.features.CreateActivity;
import eclipse.emf.editeurgraphique.risque.features.CreateJoin;
import eclipse.emf.editeurgraphique.risque.features.CreateConnection;
import eclipse.emf.editeurgraphique.risque.features.CreateEndEvent;
import eclipse.emf.editeurgraphique.risque.features.CreateBranch;
import eclipse.emf.editeurgraphique.risque.features.CreateMerge;
import eclipse.emf.editeurgraphique.risque.features.CreateFork;
import eclipse.emf.editeurgraphique.risque.features.CreateStartEvent;

public class RisqueFeatureProvider extends DefaultFeatureProvider {

	public RisqueFeatureProvider(IDiagramTypeProvider dtp) {
		super(dtp);
	}

	@Override
	public ICreateFeature[] getCreateFeatures() {
		return new ICreateFeature[] { new CreateStartEvent(this),
				new CreateEndEvent(this), new CreateActivity(this),
				new CreateMerge(this),
				new CreateBranch(this),
				new CreateJoin(this), new CreateFork(this) };
	}

	@Override
	public IAddFeature getAddFeature(IAddContext context) {
		if (context.getNewObject() instanceof StartEvent) {
			return new AddStartEvent(this);
		} else if (context.getNewObject() instanceof EndEvent) {
			return new AddEndEvent(this);
		//} else if (context.getNewObject() instanceof EReference) {
			//return new AddConnection(this);
		} else if (context.getNewObject() instanceof Activity) {
			return new AddActivity(this);
		} else if (context.getNewObject() instanceof Merge) {
			return new AddMerge(this);
		} else if (context.getNewObject() instanceof Branch) {
			return new AddBranch(this);
		} else if (context.getNewObject() instanceof Fork) {
			return new AddFork(this);
		} else if (context.getNewObject() instanceof Join) {
			return new AddJoin(this);
		} else if (context.getNewObject() instanceof SequenceFlow) {
			return new AddConnection(this);
		}

		return super.getAddFeature(context);
	}

	@Override
	public ICreateConnectionFeature[] getCreateConnectionFeatures() {
		return new ICreateConnectionFeature[] { new CreateConnection(this) };
	}

	@Override
	public IFeature[] getDragAndDropFeatures(IPictogramElementContext context) {
		// simply return all create connection features
		return getCreateConnectionFeatures();
	}

}