package eclipse.emf.editeurgraphique.risque.diagram;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;

public class RisqueDiagramTypeProvider extends AbstractDiagramTypeProvider {
	// private IToolBehaviorProvider[] toolBehaviorProviders;

	public RisqueDiagramTypeProvider() {
		super();
		setFeatureProvider(new RisqueFeatureProvider(this));
	}

	// @Override
	// public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
	// if (toolBehaviorProviders == null) {
	// toolBehaviorProviders = new IToolBehaviorProvider[] { new
	// EditeurGraphiqueToolBehaviorProvider(
	// this) };
	// }
	// return toolBehaviorProviders;
	// }

}
