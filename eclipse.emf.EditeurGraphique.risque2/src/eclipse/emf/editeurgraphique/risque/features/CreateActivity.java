package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Activity;
import eclipse.emf.editeurgraphique.model.editeurgraphique.ActivityType;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;

public class CreateActivity extends AbstractCreateFeature {

	public CreateActivity(IFeatureProvider fp) {
		super(fp, "Activity", "Create Activity");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Create Model Instance
		Activity activity = EditeurgraphiqueFactory.eINSTANCE.createActivity();
		activity.setActivityType(ActivityType.TASK);

		// Associate a Graphical Representation to Step
		addGraphicalRepresentation(context, activity);

		return new Object[] { activity };
	}
}