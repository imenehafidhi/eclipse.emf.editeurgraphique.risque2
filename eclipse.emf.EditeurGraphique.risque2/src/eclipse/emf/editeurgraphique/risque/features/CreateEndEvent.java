package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EndEvent;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EventType;

public class CreateEndEvent extends AbstractCreateFeature {

	public CreateEndEvent(IFeatureProvider fp) {
		super(fp, "End Event", "Create End Event");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Create Model Instance
		EndEvent endEvent = EditeurgraphiqueFactory.eINSTANCE.createEndEvent();
		endEvent.setEventType(EventType.END);

		// Associate a Graphical Representation to Step
		addGraphicalRepresentation(context, endEvent);

		return new Object[] { endEvent };
	}
}
