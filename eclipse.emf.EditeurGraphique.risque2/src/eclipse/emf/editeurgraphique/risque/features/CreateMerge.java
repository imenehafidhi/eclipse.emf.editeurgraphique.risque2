package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EndEvent;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EventType;
import eclipse.emf.editeurgraphique.model.editeurgraphique.GatewayType;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Merge;

public class CreateMerge extends AbstractCreateFeature {

	public CreateMerge(IFeatureProvider fp) {
		super(fp, "Merge", "Create Merge");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Create Model Instance
		Merge merge = EditeurgraphiqueFactory.eINSTANCE.createMerge();
		merge.setGatewayType(GatewayType.MERGE);

		// Associate a Graphical Representation to Step
		addGraphicalRepresentation(context, merge);

		return new Object[] { merge };
	}
}
