package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EventType;
import eclipse.emf.editeurgraphique.model.editeurgraphique.StartEvent;

public class CreateStartEvent extends AbstractCreateFeature {

	public CreateStartEvent(IFeatureProvider fp) {
		super(fp, "Start Event", "Create Start Event");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Create Model Instance
		StartEvent startEvent = EditeurgraphiqueFactory.eINSTANCE
				.createStartEvent();
		startEvent.setEventType(EventType.START);

		// Associate a Graphical Representation to Step
		addGraphicalRepresentation(context, startEvent);

		return new Object[] { startEvent };
	}
}
