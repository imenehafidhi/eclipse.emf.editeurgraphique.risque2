package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Branch;
import eclipse.emf.editeurgraphique.model.editeurgraphique.GatewayType;


public class CreateBranch extends AbstractCreateFeature {

	public CreateBranch(IFeatureProvider fp) {
		super(fp, "Branch", "Create Branch");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Create Model Instance
		Branch branch = EditeurgraphiqueFactory.eINSTANCE.createBranch();
		branch.setGatewayType(GatewayType.BRANCH);

		// Associate a Graphical Representation to Step
		addGraphicalRepresentation(context, branch);

		return new Object[] {branch };
	}
}
