package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;
import eclipse.emf.editeurgraphique.model.editeurgraphique.Fork;
import eclipse.emf.editeurgraphique.model.editeurgraphique.GatewayType;



public class CreateFork extends AbstractCreateFeature {

	public CreateFork (IFeatureProvider fp) {
		super(fp, "Fork ", "Create Fork ");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Create Model Instance
		Fork  fork = EditeurgraphiqueFactory.eINSTANCE
				.createFork();
		fork.setGatewayType(GatewayType.FORK);

		// Associate a Graphical Representation to Step
		addGraphicalRepresentation(context, fork);

		return new Object[] { fork };
	}
}
