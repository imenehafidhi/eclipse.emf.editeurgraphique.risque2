package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;


import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;
import eclipse.emf.editeurgraphique.model.editeurgraphique.GatewayType;
import eclipse.emf.editeurgraphique.model.editeurgraphique.Join;


public class CreateJoin extends AbstractCreateFeature {

	public CreateJoin(IFeatureProvider fp) {
		super(fp, "Join", "Create Join");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		return context.getTargetContainer() instanceof Diagram;
	}

	@Override
	public Object[] create(ICreateContext context) {
		// Create Model Instance
		Join join = EditeurgraphiqueFactory.eINSTANCE
				.createJoin();
		join.setGatewayType(GatewayType.JOIN);

		// Associate a Graphical Representation to Step
		addGraphicalRepresentation(context, join);

		return new Object[] { join};
	}
}
