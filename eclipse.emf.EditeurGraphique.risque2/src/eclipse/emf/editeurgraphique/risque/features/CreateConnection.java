package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Activity;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurGraphiqueElement;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EditeurgraphiqueFactory;
import eclipse.emf.editeurgraphique.model.editeurgraphique.EndEvent;
import eclipse.emf.editeurgraphique.model.editeurgraphique.SequenceFlow;
import eclipse.emf.editeurgraphique.model.editeurgraphique.StartEvent;

public class CreateConnection extends AbstractCreateConnectionFeature {

	private Anchor sourceAnchor = null;
	private Anchor targetAnchor = null;

	public CreateConnection(IFeatureProvider fp) {
		super(fp, "EReference", "Create EReference");
	}

	@Override
	public boolean canStartConnection(ICreateConnectionContext context) {
		try {
			// return true if start anchor belongs to a EClass
			EditeurGraphiqueElement source = getEditeurGraphiqueElement(context.getSourceAnchor());
			if (source != null) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean canCreate(ICreateConnectionContext context) {
		try {
			// return true if both anchors belong to an EClass
			// and those EClasses are not identical
			EditeurGraphiqueElement  source = getEditeurGraphiqueElement(context.getSourceAnchor());
			EditeurGraphiqueElement target = getEditeurGraphiqueElement(context.getTargetAnchor());
			if (source != null && target != null && source != target) {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
	@Override
	public Connection create(ICreateConnectionContext context) {
		try {
			Connection newConnection = null;
	
			// get EClasses which should be connected
			EditeurGraphiqueElement source = getEditeurGraphiqueElement(context.getSourceAnchor());
			EditeurGraphiqueElement target = getEditeurGraphiqueElement(context.getTargetAnchor());
	
			if (source != null && target != null) {
				// create new business object 
				SequenceFlow eReference = createSequenceFlow(source, target);
				// add connection for business object
				AddConnectionContext addContext = new AddConnectionContext(context.getSourceAnchor(), context.getTargetAnchor());
				addContext.setNewObject(eReference);
				newConnection = (Connection) getFeatureProvider().addIfPossible(addContext);
				
				//addGraphicalRepresentation(addContext, eReference);
			}
			
	    	// set the state of editor to dirty
	    	//DiagramUtil.setActiveEditorStateToDirty();
	
			return newConnection;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	private EditeurGraphiqueElement getEditeurGraphiqueElement(Anchor anchor) {
		try {
			if (anchor != null) {
				Object object = getBusinessObjectForPictogramElement(anchor.getParent());
				if (object instanceof EditeurGraphiqueElement) {
					return (EditeurGraphiqueElement) object;
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	private StartEvent getStartEvent(Anchor anchor) {
		if (anchor != null) {
			Object object = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (object instanceof StartEvent) {
				return (StartEvent) object;
			}
		}
		return null;
	}

	private EndEvent getEndEvent(Anchor anchor) {
		if (anchor != null) {
			Object object = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (object instanceof EndEvent) {
				return (EndEvent) object;
			}
		}
		return null;
	}

	private Activity getActivity(Anchor anchor) {
		if (anchor != null) {
			Object object = getBusinessObjectForPictogramElement(anchor
					.getParent());
			if (object instanceof Activity) {
				return (Activity) object;
			}
		}
		return null;
	}

	// private Branch getBranch(Anchor anchor) {
	// if (anchor != null) {
	// Object object = getBusinessObjectForPictogramElement(anchor
	// .getParent());
	// if (object instanceof Branch) {
	// return (Branch) object;
	// }
	// }
	// return null;
	// }
	//
	// private Merge getMerge(Anchor anchor) {
	// if (anchor != null) {
	// Object object = getBusinessObjectForPictogramElement(anchor
	// .getParent());
	// if (object instanceof Merge) {
	// return (Merge) object;
	// }
	// }
	// return null;
	// }
	//
	// private Fork getFork(Anchor anchor) {
	// if (anchor != null) {
	// Object object = getBusinessObjectForPictogramElement(anchor
	// .getParent());
	// if (object instanceof Fork) {
	// return (Fork) object;
	// }
	// }
	// return null;
	// }
	//
	// private Join getJoin(Anchor anchor) {
	// if (anchor != null) {
	// Object object = getBusinessObjectForPictogramElement(anchor
	// .getParent());
	// if (object instanceof Join) {
	// return (Join) object;
	// }
	// }
	// return null;
	// }
	//
	private EReference createEReference(Activity source, StartEvent target) {
		EReference eReference = EcoreFactory.eINSTANCE.createEReference();
		eReference.setName("new EReference");
		// eReference.setEType(target);
		eReference.setLowerBound(0);
		eReference.setUpperBound(1);
		// source.getEStructuralFeatures().add(eReference);
		return eReference;
	}
	public SequenceFlow createSequenceFlow(EditeurGraphiqueElement source, EditeurGraphiqueElement target) {
		try {
			SequenceFlow link = EditeurgraphiqueFactory.eINSTANCE.createSequenceFlow();
			link.setName("");
			link.setSourceRef(source);
			link.setTargetRef(target);
			source.getOutgoing().add(link);
			target.getIncoming().add(link);
			
			//link.setSource(source);
			//target.getIncomingLinks().add(link);
			
			/*eReference.setEType(target);
			eReference.setLowerBound(0);
			eReference.setUpperBound(1);
			source.getEStructuralFeatures().add(eReference);*/
			
			return link;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
}

