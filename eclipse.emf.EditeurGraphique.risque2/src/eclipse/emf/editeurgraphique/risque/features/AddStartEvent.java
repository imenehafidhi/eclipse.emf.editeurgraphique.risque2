package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddFeature;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.BoxRelativeAnchor;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Activity;
import eclipse.emf.editeurgraphique.model.editeurgraphique.StartEvent;

public class AddStartEvent extends AbstractAddFeature {

	public AddStartEvent(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canAdd(IAddContext context) {
		if (context.getTargetContainer() instanceof Diagram) {
			if (context.getNewObject() instanceof StartEvent) {
				return true;
			}
		}
		return false;
	}

	
	public PictogramElement add(IAddContext context) {
        StartEvent addedClass = (StartEvent) context.getNewObject();
        Diagram targetDiagram = (Diagram) context.getTargetContainer();
 
        // CONTAINER SHAPE WITH ROUNDED RECTANGLE
        IPeCreateService peCreateService = Graphiti.getPeCreateService();
        ContainerShape containerShape =
             peCreateService.createContainerShape(targetDiagram, true);
 
        final int objectWidth = 50;
		final int objectHeight = 50;
        IGaService gaService = Graphiti.getGaService();
 
        {	
            // create and set graphics algorithm
        	Rectangle invisibleRectangle = gaService
    				.createInvisibleRectangle(containerShape);
    		invisibleRectangle.setFilled(false);
    		invisibleRectangle.setLineVisible(false);
    		gaService.setLocationAndSize(invisibleRectangle, context.getX(),
    				context.getY(), objectWidth, objectHeight);
    		
 
            // if added Class has no resource we add it to the resource 
            // of the diagram
            // in a real scenario the business model would have its own resource
            if (addedClass.eResource() == null) {
                     getDiagram().eResource().getContents().add(addedClass);
            }
            // create link and wire it
            link(containerShape, addedClass);
        }
            Shape circleShape = peCreateService.createShape(containerShape, false);
    		Ellipse circle = gaService.createEllipse(circleShape);
    		circle.setFilled(false);
    		gaService.setLocationAndSize(circle, 0, 0, objectWidth, objectHeight);
    		link(circleShape, addedClass);

    		if (addedClass.eResource() == null) {
    			getDiagram().eResource().getContents().add(addedClass);
    		}

    		// create box relative anchor
    		final BoxRelativeAnchor topBoxAnchor = peCreateService
    				.createBoxRelativeAnchor(containerShape);
    		final Ellipse topCircle = gaService.createPlainEllipse(topBoxAnchor);
    		topCircle.setBackground(manageColor(new ColorConstant(255, 255, 255)));
    		gaService.setLocationAndSize(topCircle, objectWidth, 0, 5, 5);

    		layoutPictogramElement(containerShape);
    		 {
    	            // create shape for text
    	            Shape shape = peCreateService.createShape(containerShape, false);
    	 
    	            // create and set text graphics algorithm
    	            Text text = gaService.createDefaultText(getDiagram(), shape,
    	                        addedClass.getName());
    	            text.setForeground(manageColor(IColorConstant.BLACK));
    	            text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
    	            text.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
    	            //text.getFont().setBold(true);
    	            gaService.setLocationAndSize(text, 0, 0, objectWidth, 20);
    	 
    	            // create link and wire it
    	            link(shape, addedClass);
    	        }
    		 peCreateService.createChopboxAnchor(containerShape);
    		  
    	        // call the layout feature
    	        layoutPictogramElement(containerShape);

    		return containerShape;
    	}
	
	}
