package eclipse.emf.editeurgraphique.risque.features;

import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractAddFeature;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaCreateService;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.services.IPeService;
import org.eclipse.graphiti.util.IColorConstant;

import eclipse.emf.editeurgraphique.model.editeurgraphique.EndEvent;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Gateway;

import eclipse.emf.editeurgraphique.model.editeurgraphique.Merge;

public class AddMerge extends AbstractAddFeature {

	final static IGaService gaService = Graphiti.getGaService();
	public AddMerge(IFeatureProvider fp) {
		super(fp);
	}
	@Override
	public boolean canAdd(IAddContext context) {
		if (context.getTargetContainer() instanceof Diagram) {
			if (context.getNewObject() instanceof Merge) {
				return true;
			}
		}
		return false;
	}
	@Override
	public PictogramElement add(IAddContext context) {
        Merge addedClass = (Merge) context.getNewObject();
        Diagram targetDiagram = (Diagram) context.getTargetContainer();
 
        // CONTAINER SHAPE WITH ROUNDED RECTANGLE
        IPeCreateService peCreateService = Graphiti.getPeCreateService();
        ContainerShape containerShape =
             peCreateService.createContainerShape(targetDiagram, true);
 
        final int objectWidth = 50;
		final int objectHeight = 50;
        IGaService gaService = Graphiti.getGaService();
 
        {	
            // create and set graphics algorithm
        	Rectangle invisibleRectangle = gaService
    				.createInvisibleRectangle(containerShape);
    		invisibleRectangle.setFilled(false);
    		invisibleRectangle.setLineVisible(false);
    		gaService.setLocationAndSize(invisibleRectangle, context.getX(),
    				context.getY(), objectWidth, objectHeight);
    		
 
            // if added Class has no resource we add it to the resource 
            // of the diagram
            // in a real scenario the business model would have its own resource
            if (addedClass.eResource() == null) {
                     getDiagram().eResource().getContents().add(addedClass);
            }
            // create link and wire it
            link(containerShape, addedClass);
        }
		
        Shape gatewayShape = peCreateService.createShape(containerShape, false);
		Polygon gatewayPolygon = createGateway(gatewayShape,objectWidth,objectHeight);
		gatewayPolygon.setFilled(false);
		gaService.setLocationAndSize(gatewayPolygon, 0, 0, objectWidth, objectHeight);
		link(gatewayShape, addedClass);

		if (addedClass.eResource() == null) {
			getDiagram().eResource().getContents().add(addedClass);
		}

		peCreateService.createChopboxAnchor(containerShape);
		  
        // call the layout feature
        layoutPictogramElement(containerShape);
		return containerShape;
	}
	
	public static Polygon createGateway(Shape container, 	final int objectWidth,	final int objectHeight) {
		final int widthRadius = objectWidth / 2;
		final int heightRadius = objectHeight / 2;
		final int[] gateWayPoints = {0, heightRadius, widthRadius, 0, 2 * widthRadius, heightRadius, widthRadius, 2 * heightRadius};
		return gaService.createPolygon(container, gateWayPoints);
	}

	
	
}